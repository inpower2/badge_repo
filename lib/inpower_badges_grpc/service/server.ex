defmodule InpowerBadgesGrpc.Service.Server do
  use GRPC.Server, service: Badgeapi.BadgeService.Service

  alias Badgeapi.{
    CreateBadgeRequest,
    CreateBadgeResponse,

    GetBadgeRequest,
    GetBadgeResponse,
    GetBadgeRequestAll,
    GetBadgeResponseAll,

    DeleteBadgeRequest,
    DeleteBadgeResponse,

    UpdateBadgeRequest,
    UpdateBadgeResponse

  }

  alias InpowerBadge.HandleDb

  require Logger

  ####################### badges #####################################################

  def get_badge(%GetBadgeRequest{id: id}, _stream) do
    with getting_badges <- HandleDb.get_badges(id) do

      badges =
      getting_badges
      |> Enum.map(fn(x) -> Map.from_struct(x) end)


      GetBadgeResponseAll.new(badges: badges)
    else
      _error ->
        Logger.info("Did not find arguments ")
        GetBadgeResponseAll.new()
    end
  end

  def get_badge_all(%GetBadgeRequestAll{}, _stream) do
    with getting_badges <- HandleDb.get_all_badges() do

      badges =
      getting_badges
      |> Enum.map(fn(x) -> Map.from_struct(x) end)


      GetBadgeResponseAll.new(badges: badges)
    else
      _error ->
        Logger.info("Did not find arguments ")
        GetBadgeResponseAll.new()
    end
  end

  def get_badge(%GetBadgeRequest{id: id}, _stream) do
    with getting_badges <- HandleDb.get_badges_by_id(id) do
      badges =
      getting_badges
      |> Enum.map(fn(x) -> Map.from_struct(x) end)
      GetBadgeResponseAll.new(badges: badges)
    else
      _error ->
        Logger.info("Did not find arguments #{id}")
        GetBadgeResponseAll.new()
    end
  end

  def create_badge(%CreateBadgeRequest{name: name, order: order, video_url: video_url , image: image}, _stream) do
    with {:ok, badges} <- HandleDb.create_badges_in_Table(name, order, video_url , image) do
      # IO.inspect(badges, label: "here is badges")
      CreateBadgeResponse.new(badges: badges)
    else
      _error ->
        Logger.info("Did not find arguments #{name}")
        CreateBadgeResponse.new()
    end
  end

  def update_badge(%UpdateBadgeRequest{id: id, name: name, order: order, video_url: video_url , image: image}, _stream) do
    with {:ok, updating_badges} <- HandleDb.update_badges_in_table(id, name, order, video_url , image) do
      UpdateBadgeResponse.new(updating_badges: updating_badges)
    else
      _error ->
        Logger.info("Did not find arguments #{id}")
        UpdateBadgeResponse.new()
    end
  end

  def delete_badge(%DeleteBadgeRequest{id: id}, _stream) do

    with {1, deleting_badges} <- HandleDb.delete_badges_by_id(id) do
      case deleting_badges do
        nil ->
          DeleteBadgeResponse.new(status: true)
        _ ->
          DeleteBadgeResponse.new(status: false)
      end

    else
      _error ->
        Logger.info("Did not find arguments #{id}")
        DeleteBadgeResponse.new()
    end
  end


end
