defmodule InpowerBadgesGrpc.Endpoint do
  use GRPC.Endpoint

  intercept(GRPC.Logger.Server)
  run(InpowerBadgesGrpc.Service.Server)
end
