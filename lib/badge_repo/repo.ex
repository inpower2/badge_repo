defmodule BadgeRepo.Repo do
  use Ecto.Repo,
    otp_app: :badge_repo,
    adapter: Ecto.Adapters.Postgres
end
