defmodule BadgeRepo.Badges.Badge do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}

  schema "badges" do
    field :name, :string
    field :order, :string
    ##field :group_id, :string
    field :image, :string
    field :video_url , :string
    timestamps()
  end

  @doc false
  def changeset(badge, attrs) do
    badge
    |> cast(attrs, [:name, :order, :video_url, :image])
    |> validate_required([:name])
  end
end
