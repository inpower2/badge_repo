defmodule BadgeRepoWeb.Router do
  use BadgeRepoWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", BadgeRepoWeb do
    pipe_through :api
  end
end
