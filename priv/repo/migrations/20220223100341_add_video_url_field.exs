defmodule BadgeRepo.Repo.Migrations.AddVideoUrlField do
  use Ecto.Migration

  def change do
    alter table("badges") do
      add :video_url, :string
      remove :group_id
    end


  end
end
