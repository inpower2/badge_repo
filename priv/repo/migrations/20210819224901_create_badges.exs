defmodule BadgeRepo.Repo.Migrations.CreateBadges do
  use Ecto.Migration

  def change do
    create table(:badges, primary_key: false ) do
      add :name, :string
      add :order, :string
      add :group_id, :string
      add :image, :string
      add :id, :uuid, primary_key: true

      timestamps()
    end

  end
end
