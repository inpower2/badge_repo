use Mix.Config

# Configure your database
config :badge_repo, BadgeRepo.Repo,
  username: System.get_env("POSTGRES_USER"),
  password: System.get_env("POSTGRES_PASSWORD"),
  database: System.get_env("POSTGRES_DB_PROD"),
  hostname: System.get_env("POSTGRES_HOST"),
  show_sensitive_data_on_connection_error: true,
  pool_size: 10
