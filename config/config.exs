# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :badge_repo,
  ecto_repos: [BadgeRepo.Repo]

config :grpc, start_server: true
# Configures the endpoint
# config :badge_repo, BadgeRepoWeb.Endpoint,
#   url: [host: "localhost"],
#   secret_key_base: "i57DJAUFYARRpt9TZG1X1UfdG43k6p65qOqYrHG3Ai1IFCzlH86b9aPZ8hFdAZ94",
#   render_errors: [view: BadgeRepoWeb.ErrorView, accepts: ~w(json)],
#   pubsub: [name: BadgeRepo.PubSub, adapter: Phoenix.PubSub.PG2]

# # Configures Elixir's Logger
# config :logger, :console,
#   format: "$time $metadata[$level] $message\n",
#   metadata: [:request_id]

# # Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :ex_aws,
  debug_requests: true,
  access_key_id: "AKIA5PMTBW2NHMXC7VEX",
  secret_access_key: "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",
  s3: [
    scheme: "https://",
    host: "inpower2.s3.amazonaws.com",
    region: "us-east-2"
  ]

config :inpower_s3,
  bucket: "inpower2",
  region: "us-east-2",
  webroot: "inpower_comments"


# # Import environment specific config. This must remain at the bottom
# # of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
